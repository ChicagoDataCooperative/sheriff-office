Data in this repository was collected by [Chicago Appleseed Fund for Justice](http://www.chicagoappleseed.org/) as part of the work of the Coalition to End Money Bond.

It includes

1. Sheriff’s Daily population reports, newly available on the Sheriff’s website, which show the daily numbers of people in the jail, and what division they are in, plus the number of people on EM (“community corrections”), but nothing about demographics, pretrial/sentenced, bond status, etc. We have these reports from August 18-October 18, November 6, 13, 20, and 27, and daily data from December 1 and on are available on the Sheriff’s website.
2. Sheriff’s Weekly population reports from CCJ (via FOIA from Sheriff's office -- shows populations on EM and in CCJ, and the “bookings, discharges, and shipments” along with weekly averages and a YTD summary of weekly populations. This report also has no breakdown by pretrial versus sentenced, bond status, or demographic information; does contain weekly averages YTD, etc.) We have these for November 6, 13, 20, and 27, December 4, 11, and 18, 2017, and January 1, 15, 22, 2018.
3. Jail Population Data from FOIA requests to the Sheriff, showing the racial and gender breakdowns of the daily jail populations from August 18, 2017 to December 15, 2017. The racial and gender breakdowns are also broken out by whether a person is incarcerated based on a unpaid money bond, no bond, on EM, or sentenced to a jail term.
4. Daily (weekday) CCJ population breakdown according to bond status (obtained via FOIA request from Sheriff's office; shows who is held in CCJ with specific numbers for people with unpaid monetary bonds, people held no-bond, and people with EM orders that haven't been released on EM.) We have this for August 18-December 15, 2017 and will request additional dates in the future.

## FOIAs:

### Breakdown by bond status
> I request the following records for the dates of 8/18/2017 to 11/18/2017:
>
>    The daily number and percentage of the confined pretrial population with a D or a C bond.
>    The daily number and percentage of the confined pretrial population held on No Bond.
>
> Please note that I have expanded the time frame to reflect the time that has passed since I submitted my initial request. Please also note that I am requesting tallies for each of the days that fall within this time window. 
>
> Please let me know when I can respect a response.

### Jail population by demographics
> Thank you for being responsive to my other data requests. I
> am submitting another request for public records under the Freedom of Information Act (FOIA).
>
> I request any and all records, in electronic form, showing
> the following information for the dates of 8/18/2017 to 12/18/2017:
>
>    The daily confined pretrial population with a D or C bond, broken down by gender,
>     race/ethnicity, and age. 
>
>    The daily confined pretrial population held on No Bond, broken down by gender,
>     race/ethnicity, and age. 
>
>    The daily confined population serving sentences in the jail, broken down by gender,
>     race/ethnicity, and age. 
>
>    The daily pretrial population of people on electronic monitoring with a bond,
>     broken down by gender, race/ethnicity, and age. 
>
>    The daily pretrial population of people on electronic monitoring with No Bond,
>     broken down by gender, race/ethnicity, and age. 
>
> I request that all of these data points be broken down into
> charge classifications (Class 1, Class 2, Class 3, Class 4, Class X, Murder, Felony Unclassified, Misdemeanor, Missing/Unknown).
>
> The law allows you to impose a fee waiver when information
> is sought in the public interest, as is the case for this request. I am submitting this request in my capacity as a journalist, and my request is for news gathering purposes. The topic of jail population is of public interest, and the sheriff has issued statements
> highlighting the jail population, so it’s important for the public to better understand that information. Therefore, I would like to request a fee waiver.

